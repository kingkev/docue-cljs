# Docue - Document Management System Frontend

tl;dr; [Docue Frontend](https://github.com/kevgathuku/docue-frontend) rewritten in ClojureScript

## Dependencies and Setup

### Node.js

Install [Node.js](http://nodejs.org/) and node dependencies:

```
npm install
```

Download all of the 3rd-party javascript dependencies:

```
node_modules/.bin/bower install
```

### Starting the Server
 - Dev mode: `boot dev`
